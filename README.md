# Teamleader Test | 02 - Ordering

> **Note:** This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Requirements

- [Node LTS v8.9.1](https://nodejs.org/en/)
- NPM (bundled with the Node installation)

## Install

Clone the repo

```
git clone git@gitlab.com:Flovan/teamleader-test.git
cd teamleader-test
```

Install the dependencies

```
npm i
```

## Usage

For development:

```
npm start
```

Runs the test watcher in an interactive mode.

To make a production build:

```
npm run build
```

Builds the app for production to the build folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.
