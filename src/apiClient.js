/**
 * Get orders
 * @return {Promise}
 */
export const getOrders = async () => {
  try {
    return Promise.all([
      fetch('./data/order1.json'),
      fetch('./data/order2.json'),
      fetch('./data/order3.json')
    ]).then(async orders => {
      const data = [];
      orders.map(async order => {
        const orderData = await order.json();
        data.push(orderData);
      });
      return data;
    });
  } catch (err) {
    throw new Error('Getting orders failed');
  }
};

/**
 * Get customers
 * @return {Promise}
 */
export const getCustomers = async () => {
  try {
    return fetch('./data/customers.json').then(async customers => {
      const customersData = await customers.json();
      return customersData;
    });
  } catch (err) {
    throw new Error('Getting customers failed');
  }
};

/**
 * Get products
 * @return {Promise}
 */
export const getProducts = async () => {
  try {
    return fetch('./data/products.json').then(async products => {
      const productsData = await products.json();
      return productsData;
    });
  } catch (err) {
    throw new Error('Getting products failed');
  }
};

/**
 * Get products
 * @return {Promise}
 */
export const saveOrder = async order => {
  try {
    // This should be a POST fetch
    console.log(order);
    return Promise.resolve();
  } catch (err) {
    throw new Error('Failed to save product failed');
  }
};
