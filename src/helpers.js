export const decimalPad = (dec, len = 2, chr = '0') => {
  dec = dec.toString();
  let p = dec.indexOf('.');
  if (p < 0) return `${dec}.00`;
  p = p !== -1 ? dec.length - p - 1 : -1;
  for (var m = p; m < len; m++) dec += chr;
  return dec;
};

export const round = (num, decimals = 2) => {
  const factor = Math.pow(10, decimals);
  return Math.round(num * factor) / factor;
};
