import React from 'react';
import PropTypes from 'prop-types';
import { decimalPad } from '../../helpers';

const Overview = ({ orders, onEdit }) => (
  <div>
    <header>
      <h1>Order overview</h1>
    </header>
    {!!orders.length && (
      <table>
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Client</th>
            <th className="align-right">Total</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {orders.map(order => (
            <tr key={`order-${order.id}`}>
              <td>#{order.id}</td>
              <td>{order.customer.name}</td>
              <td className="align-right">€{decimalPad(order.total)}</td>
              <td>
                <button onClick={() => onEdit(order.id)}>Edit</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    )}
  </div>
);

Overview.propTypes = {
  orders: PropTypes.array,
  onEdit: PropTypes.func.isRequired
};
Overview.defaultProps = {
  orders: []
};

export default Overview;
