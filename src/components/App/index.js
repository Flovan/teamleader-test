import React, { Component } from 'react';
import {
  getOrders,
  getCustomers,
  getProducts,
  saveOrder
} from '../../apiClient';
import Overview from '../Overview';
import Order from '../Order';
import Catalog from '../Catalog';
import { round } from '../../helpers';

class App extends Component {
  constructor(props) {
    super();

    this.state = {
      globalMessage: {
        type: 'info',
        message: 'Loading...'
      },
      orders: [],
      products: [],
      selectedOrder: null
    };

    this.handleOrderEdit = this.handleOrderEdit.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleMakeOrder = this.handleMakeOrder.bind(this);
    this.handleRemoveFromOrder = this.handleRemoveFromOrder.bind(this);
    this.handleAddToOrder = this.handleAddToOrder.bind(this);
  }

  async componentWillMount() {
    try {
      const orders = await getOrders();
      const customers = await getCustomers();
      let products = await getProducts();

      // Note: these manipulations should happen server side
      products = products.map(product => ({
        ...product,
        price: parseFloat(product.price)
      }));

      orders.forEach(order => {
        order.customer = customers.find(
          customer => customer.id === order['customer-id']
        );
        order.total = parseFloat(order.total);
        order.items = order.items.map(item => {
          const newItem = {
            ...item,
            ...products.find(product => product.id === item['product-id']),
            quantity: parseInt(item.quantity, 10),
            total: parseFloat(item.total)
          };
          return newItem;
        });
      });

      this.setState({ globalMessage: null, orders, products });
    } catch (err) {
      this.setState({
        globalMessage: {
          type: 'error',
          message: err.message
        }
      });
    }
  }

  handleOrderEdit(id) {
    this.setState({
      selectedOrder: this.state.orders.find(order => order.id === id)
    });
  }

  handleBack() {
    this.setState({
      selectedOrder: null,
      globalMessage: null
    });
  }

  async handleMakeOrder(id) {
    try {
      await saveOrder(this.state.orders.find(order => order.id === id));
      this.setState({
        globalMessage: {
          type: 'success',
          message: `The order (ID ${id}) has been saved!`
        }
      });
    } catch (err) {
      this.setState({
        globalMessage: {
          type: 'error',
          message: err.message
        }
      });
    }
  }

  handleRemoveFromOrder(orderId, productIndex) {
    const orders = this.state.orders.map(order => {
      if (order.id !== orderId) return order;
      const newOrder = {
        ...order,
        items: order.items.filter((product, index) => index !== productIndex)
      };
      newOrder.total = 0;
      newOrder.items.forEach(product => {
        newOrder.total += product.price * product.quantity;
      }, 0);
      newOrder.total = round(newOrder.total);
      return newOrder;
    });

    this.setState({ orders });
  }

  handleAddToOrder(orderId, newProduct, quantity) {
    const orders = [];
    this.state.orders.forEach((order, index) => {
      let newOrder = { ...order };

      if (order.id === orderId) {
        const total = round(newProduct.price * quantity);
        let alreadyAdded = false;
        newOrder.items.forEach(product => {
          if (newProduct.id === product.id) {
            alreadyAdded = true;
            product.quantity += quantity;
            product.total = round(product.total + total);
          }
        });
        if (!alreadyAdded) {
          newOrder.items.push({
            'product-id': newProduct.id,
            unitPrice: newProduct.price,
            ...newProduct,
            quantity,
            total
          });
        }
        newOrder.total = round(newOrder.total + total);
      }
      orders[index] = newOrder;
    });
    this.setState({ orders });
  }

  render() {
    const { globalMessage, orders, selectedOrder, products } = this.state;

    return (
      <main>
        {globalMessage && (
          <div className={`global-message ${globalMessage.type}`}>
            <p>{globalMessage.message}</p>
          </div>
        )}
        {!selectedOrder && (
          <Overview orders={orders} onEdit={this.handleOrderEdit} />
        )}
        {!!selectedOrder && (
          <div>
            <Order
              order={orders.find(order => order.id === selectedOrder.id)}
              onBack={this.handleBack}
              onMakeOrder={this.handleMakeOrder}
              onRemoveFromOrder={this.handleRemoveFromOrder}
            />
            <Catalog
              products={products}
              onAddToOrder={(product, quantity) =>
                this.handleAddToOrder(selectedOrder.id, product, quantity)
              }
            />
          </div>
        )}
      </main>
    );
  }
}

export default App;
