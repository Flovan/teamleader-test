import React, { Component } from 'react';
// import PropTypes from 'prop-types';

class AmountField extends Component {
  // static propTypes = {};
  // static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = { amount: 1 };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const val = e.target.value || 1;
    if (!/[0-9]/.test(val)) {
      e.preventDefault();
      return;
    }
    this.setState({ amount: val });
  }

  render() {
    return (
      <input
        type="text"
        {...this.props}
        onChange={this.handleChange}
        value={this.state.amount}
      />
    );
  }
}

export default AmountField;
